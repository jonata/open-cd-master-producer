#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, subprocess, tempfile, random
from PySide import QtGui, QtCore

reload(sys)
sys.setdefaultencoding('utf-8')

path_cdmp = os.path.abspath(os.path.dirname(sys.argv[0]))
path_graphics = os.path.join(path_cdmp, 'graphics')
path_pixmaps = os.path.join(path_cdmp, 'pixmaps')
path_home = os.environ.get('HOME', None)
path_tmp = os.path.join(tempfile.gettempdir(), 'cdmp-' + str(random.randint(1000,9999)))

if not os.path.isdir(path_tmp):
    os.mkdir(path_tmp)

startupinfo = None
if sys.platform == 'win32' or os.name == 'nt':
    startupinfo = subprocess.STARTUPINFO()
    startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW


class exec_generateddp_signal(QtCore.QObject):
        sig = QtCore.Signal(str)

class exec_generateddp(QtCore.QThread):
    def __init__(self, parent = None):
        QtCore.QThread.__init__(self, parent)
        self.command = []
        self.signal = exec_generateddp_signal()

    def run(self):
        final_cue = open(os.path.join(path_tmp, 'final.cue'), 'w')
        final_cue.write('PERFORMER ' + self.performer_name + '  BINARY\n')
        final_cue.write('TITLE ' + self.album_title + '  BINARY\n')
        final_cue.write('FILE final.bin  BINARY\n')

        final_bin = open(os.path.join(path_tmp, 'final.bin'), 'w')

        self.signal.sig.emit('Generating cue list and encoding audio files')

        track_number = 1
        duration = 0.0
        for audio in self.list_of_audios:
            #title = #.split('/')[-1].replace('.' + audio.split('/')[-1].split('.')[-1], '')

            self.signal.sig.emit('Generating cue list and encoding track ' + str(track_number))

            minutes = int(float(duration)/60)
            seconds = int(float(duration)%60)
            decimal = int(float(str("%.2f" % float(duration)).split('.')[-1])*.75)

            final_cue.write('  TRACK ' + str("%02d" % int(track_number)) + ' AUDIO\n')
            final_cue.write('    TITLE "' + audio[3] + '"\n')
            final_cue.write('    PERFORMER "' + audio[2] + '"\n')
            final_cue.write('    INDEX 01 ' + str("%02d" % int(minutes)) + ':' + str("%02d" % int(seconds)) + ':' + str("%02d" % int(decimal)) + '\n')

            duration += audio[-1]

            subprocess.call([os.path.join(path_cdmp, 'resources', 'sox'), audio[0], '-c', '2', '-e', 'signed-integer', '-b', '16', '-r', '44100', os.path.join(path_tmp, audio[1] + '.raw')])

            final_bin.write(open(os.path.join(path_tmp, audio[1] + '.raw')).read())

            track_number += 1

        final_cue.close()
        final_bin.close()
        #subprocess.call([os.path.join(path_cdmp, 'resources', 'sox'), '-t', 'raw', '-e', 'signed-integer', '-c', '2', '-b', '16', '-r', '44100', os.path.join(path_tmp, 'final.bin'), '-t', 'wav', '-c', '2', '-e', 'signed-integer', '-b', '16', '-r', '44100', os.path.join(path_tmp, 'final.wav')])

        self.signal.sig.emit('Generating DDP')

        if not os.path.isdir(os.path.join(self.folder, self.album_title + '_DDP')):
            os.mkdir(os.path.join(self.folder, self.album_title + '_DDP'))

        subprocess.call([os.path.join(path_cdmp, 'resources', 'cue2ddp'), '-t', os.path.join(path_tmp, 'final.cue'), os.path.join(self.folder, self.album_title + '_DDP')])

        open(os.path.join(self.folder, self.album_title + '_DDP', self.album_title + ' Log'), 'w').write(subprocess.Popen([os.path.join(path_cdmp, 'resources', 'ddpinfo'), os.path.join(self.folder, self.album_title + '_DDP')], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read())

        self.signal.sig.emit('OK')


class main_window(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setWindowTitle('CD Master Producer')
        self.resize(750, 450)
        self.setMaximumSize(self.width(), self.height())
        self.setMinimumSize(self.width(), self.height())
        self.setWindowIcon(QtGui.QIcon(os.path.join(path_pixmaps, 'cd_master_producer.png')))
        self.move((QtGui.QDesktopWidget().screenGeometry().width()-self.geometry().width())/2, (QtGui.QDesktopWidget().screenGeometry().height()-self.geometry().height())/2)

        self.list_of_audios = []
        self.performer_name = ''
        self.album_title = ''

        class main_panel(QtGui.QWidget):
            def dragEnterEvent(widget, event):
                if event.mimeData().hasUrls and len(event.mimeData().urls()) > 0:
                    event.accept()

            def dropEvent(widget, event):
                if event.mimeData().hasUrls and len(event.mimeData().urls()) > 0:
                    event.accept()
                    for filename in sorted(event.mimeData().urls()):
                        duration = float(subprocess.Popen([os.path.join(path_cdmp, 'resources', 'soxi'), '-D', filename.toLocalFile()], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read())
                        self.list_of_audios.append([filename.toLocalFile(), filename.toLocalFile().split('/')[-1].replace('.' + filename.toLocalFile().split('.')[-1], ''), self.performer_name, self.album_title, duration], )
                        # [0] = filepath
                        # [1] = filename
                        # [2] = performer
                        # [3] = title
                        # [-1] = duration
                        cdmp_list_populate(self)

        self.main_panel = main_panel(parent=self)
        self.main_panel.setAcceptDrops(True)
        self.main_panel.setGeometry(0, 0, self.width(), self.height())

        self.right_panel = QtGui.QLabel(parent=self.main_panel)
        self.right_panel.setGeometry((self.main_panel.width()*.6)-27,0,self.main_panel.width()+54,self.main_panel.height())
        self.right_panel.setObjectName('right_panel')
        self.right_panel.setStyleSheet('#right_panel { border-top: ' + str(115+((self.right_panel.height()-115)*.5)-40) + 'px; border-bottom: ' + str(((self.right_panel.height()-115)*.5)-40) + 'px; border-left: 70px; border-image: url("' + os.path.join(path_graphics, "right_panel_background.png").replace('\\', '/') + '") 2 2 2 70 stretch stretch; background-position: left center;  }')
        self.right_panel_animation = QtCore.QPropertyAnimation(self.right_panel, 'geometry')
        self.right_panel_animation.setEasingCurve(QtCore.QEasingCurve.OutCirc)

        self.right_panel_icon = QtGui.QLabel(parent=self.right_panel)
        self.right_panel_icon.setGeometry(77,self.right_panel.height()-220, 200, 200)
        #self.right_panel_icon.setStyleSheet('QLabel { background-image: url("' + os.path.join(path_graphics, "cd_icon.png").replace('\\', '/') + '") ; background-position: center bottom; background-repeat: no-repeat; }')
        self.right_panel_icon.setPixmap(QtGui.QPixmap(os.path.join(path_graphics, 'cd_icon.png')))#.scaled(self.right_panel_icon.width(), self.right_panel_icon.height(), QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation))
        self.right_panel_icon.setScaledContents(True)
        self.right_panel_icon_animation = QtCore.QPropertyAnimation(self.right_panel_icon, 'geometry')
        self.right_panel_icon_animation.setEasingCurve(QtCore.QEasingCurve.OutCirc)

        self.right_panel_label = QtGui.QLabel(parent=self.right_panel)
        self.right_panel_label.setGeometry(47,115,(self.main_panel.width()*.4)-40,80)
        self.right_panel_label.setStyleSheet('QLabel { qproperty-alignment: "AlignCenter"; color: white; }')

        self.create_ddp = QtGui.QPushButton('GENERATE\nCD MASTER', parent=self.right_panel)
        self.create_ddp.setGeometry(47,self.main_panel.height()-80,(self.main_panel.width()*.4)-40,60)
        self.create_ddp.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; color: white; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '");} QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '");}')
        self.create_ddp.clicked.connect(lambda:create_ddp(self))

        self.right_panel_wait_label = QtGui.QLabel(parent=self.right_panel)
        self.right_panel_wait_label.setGeometry(87,40,(self.main_panel.width()*.4)-40,self.main_panel.height()-80)
        self.right_panel_wait_label.setWordWrap(True)
        self.right_panel_wait_label.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size: 20pt; font-weight:; qproperty-alignment: "AlignVCenter | AlignLeft"; color: silver; }')
        self.right_panel_wait_label.setVisible(False)

        self.left_panel = QtGui.QLabel(parent=self.main_panel)
        self.left_panel.setGeometry(0,115,self.main_panel.width()*.6,self.main_panel.height()-115)
        self.left_panel.setObjectName('left_panel')
        self.left_panel.setStyleSheet('#left_panel { border-top: ' + str((self.left_panel.height()*.5)-40) + 'px; border-bottom: ' + str((self.left_panel.height()*.5)-40) + 'px; border-right: 70px; border-image: url("' + os.path.join(path_graphics, "left_panel_background.png").replace('\\', '/') + '") 2 70 2 2 stretch stretch; background-position: right center;  }')
        self.left_panel_animation = QtCore.QPropertyAnimation(self.left_panel, 'geometry')
        self.left_panel_animation.setEasingCurve(QtCore.QEasingCurve.OutCirc)

        self.cdmp_list_alert = QtGui.QLabel('There is no audio to show here.\nPlease, drag and drop the audio\nfiles here to generate the\nCD master.', parent=self.main_panel)
        self.cdmp_list_alert.setGeometry(0,115,self.main_panel.width()*.6,self.main_panel.height()-115)
        self.cdmp_list_alert.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size: 16pt; font-weight:; qproperty-alignment: "AlignCenter"; color: gray; }')

        self.cdmp_list = QtGui.QListWidget(parent=self.left_panel)
        self.cdmp_list.setGeometry(20,25,self.left_panel.width()-60,self.left_panel.height()-45)
        self.cdmp_list.setViewMode(QtGui.QListView.ListMode)
        self.cdmp_list.setStyleSheet('QListWidget { outline: none; } QListWidget::item:selected { background:#EEEEEE; }')
        self.cdmp_list.setSpacing(5)
        self.cdmp_list.setFocusPolicy(QtCore.Qt.NoFocus)
        self.cdmp_list.setIconSize(QtCore.QSize(32,32))
        #self.cdmp_list.currentItemChanged.connect(lambda:project_selected(self))

        class top_panel(QtGui.QLabel):
            def enterEvent(widget, event):
                if self.top_panel_label.isVisible():
                    widget.setStyleSheet('#top_panel { border-right: 235px; border-bottom: 65px; border-image: url("' + os.path.join(path_graphics, "top_panel_background.png").replace('\\', '/') + '") 2 235 65 2 stretch stretch; background-position: bottom right; padding-left: 20px; qproperty-alignment:  "AlignVCenter | AlignLeft";  }')
                    self.top_panel_edit_button.setVisible(True)
                    self.top_panel_clear_button.setVisible(True)

            def leaveEvent(widget, event):
                if self.top_panel_label.isVisible():
                    widget.setStyleSheet('#top_panel { border-right: 0px; border-bottom: 65px; border-image: url("' + os.path.join(path_graphics, "top_panel_background.png").replace('\\', '/') + '") 2 235 65 2 stretch stretch; background-position: bottom right; padding-left: 20px; qproperty-alignment:  "AlignVCenter | AlignLeft";  }')
                    self.top_panel_edit_button.setVisible(False)
                    self.top_panel_clear_button.setVisible(False)

        self.top_panel = top_panel(parent=self.main_panel)
        self.top_panel.setGeometry(0, 0, self.main_panel.width(), 120)
        self.top_panel.setObjectName('top_panel')
        self.top_panel.setStyleSheet('#top_panel { border-right: 0px; border-bottom: 65px; border-image: url("' + os.path.join(path_graphics, "top_panel_background.png").replace('\\', '/') + '") 2 235 65 2 stretch stretch; background-position: bottom right;  }')
        self.top_panel_animation = QtCore.QPropertyAnimation(self.top_panel, 'geometry')
        self.top_panel_animation.setEasingCurve(QtCore.QEasingCurve.OutCirc)


        self.top_panel_label = QtGui.QLabel(parent=self.top_panel)
        self.top_panel_label.setGeometry(0,0,self.top_panel.width(), self.top_panel.height())
        self.top_panel_label.setStyleSheet('QLabel { padding-left: 20px; qproperty-alignment:  "AlignVCenter | AlignLeft"; color: white; }')
        self.top_panel_label.setVisible(True)

        self.top_panel_album_title_label = QtGui.QLabel('ALBUM TITLE', parent=self.top_panel)
        self.top_panel_album_title_label.setGeometry(20,20,self.top_panel.width()-40,20)
        self.top_panel_album_title_label.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size: 8pt; font-weight:bold; qproperty-alignment: "AlignVCenter"; color: white; }')
        self.top_panel_album_title_label.setVisible(False)

        self.top_panel_album_title = QtGui.QLineEdit(parent=self.top_panel)
        self.top_panel_album_title.setGeometry(20,40,self.top_panel.width()-40,20)
        self.top_panel_album_title.setVisible(False)

        self.top_panel_performer_name_label = QtGui.QLabel('PERFORMER NAME', parent=self.top_panel)
        self.top_panel_performer_name_label.setGeometry(20,60,self.top_panel.width()-280,20)
        self.top_panel_performer_name_label.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size: 8pt; font-weight:bold; qproperty-alignment: "AlignVCenter"; color: white; }')
        self.top_panel_performer_name_label.setVisible(False)

        self.top_panel_performer_name = QtGui.QLineEdit(parent=self.top_panel)
        self.top_panel_performer_name.setGeometry(20,80,self.top_panel.width()-280,20)
        self.top_panel_performer_name.setVisible(False)

        class top_panel_edit_button(QtGui.QLabel):
            def enterEvent(widget, event):
                if self.top_panel_label.isVisible():
                    widget.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: #036; }')
                else:
                    widget.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: silver; }')

            def leaveEvent(widget, event):
                if self.top_panel_label.isVisible():
                    widget.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: white; }')
                else:
                    widget.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: #036; }')

            def mousePressEvent(widget, event):
                edit_info(self)

        self.top_panel_edit_button = top_panel_edit_button('EDIT', parent=self.top_panel)
        self.top_panel_edit_button.setGeometry(self.top_panel.width()-210, self.top_panel.height()-40, 100, 40)
        self.top_panel_edit_button.setObjectName('top_panel_edit_button')
        self.top_panel_edit_button.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: white; }')
        self.top_panel_edit_button.setVisible(False)

        class top_panel_clear_button(QtGui.QLabel):
            def enterEvent(widget, event):
                widget.setStyleSheet('#top_panel_clear_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: silver; }')

            def leaveEvent(widget, event):
                widget.setStyleSheet('#top_panel_clear_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: white; }')

            def mousePressEvent(widget, event):
                clear_info(self)

        self.top_panel_clear_button = top_panel_clear_button('CLEAR', parent=self.top_panel)
        self.top_panel_clear_button.setGeometry(self.top_panel.width()-110, self.top_panel.height()-40, 70, 40)
        self.top_panel_clear_button.setObjectName('top_panel_clear_button')
        self.top_panel_clear_button.setStyleSheet('#top_panel_clear_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: white; }')
        self.top_panel_clear_button.setVisible(False)


        self.exec_generateddp_thread = exec_generateddp()
        self.exec_generateddp_thread.signal.sig.connect(self.exec_generateddp_thread_completed)


        clear_info(self)

    def exec_generateddp_thread_completed(self, output):
        if output == 'OK':
            generate_effect(self, self.top_panel_animation, 'geometry', 500, [self.top_panel.x(),self.top_panel.y(),self.top_panel.width(),self.top_panel.height()], [self.top_panel.x(), 0,self.top_panel.width(),self.top_panel.height()])
            generate_effect(self, self.right_panel_animation, 'geometry', 500, [self.right_panel.x(),self.right_panel.y(),self.right_panel.width(),self.right_panel.height()], [(self.main_panel.width()*.6)-27,0,self.main_panel.width()+54,self.main_panel.height()])
            generate_effect(self, self.left_panel_animation, 'geometry', 500, [self.left_panel.x(),self.left_panel.y(),self.left_panel.width(),self.left_panel.height()], [0,115,self.main_panel.width()*.6,self.main_panel.height()-115])
            generate_effect(self, self.right_panel_icon_animation, 'geometry', 500, [self.right_panel_icon.x(),self.right_panel_icon.y(),self.right_panel_icon.width(),self.right_panel_icon.height()], [77,self.right_panel.height()-220, 200, 200])
            self.right_panel_label.setVisible(True)
            self.create_ddp.setVisible(True)
            self.right_panel_wait_label.setVisible(False)
        else:
            self.right_panel_wait_label.setText(output)

def clear_info(self):
    self.list_of_audios = []
    self.album_title = ''
    self.performer_name = ''

    update_info(self)

def edit_info(self):
    if self.top_panel_label.isVisible():
        #self.top_panel_label.setVisible(False)
        self.top_panel_edit_button.setText('CONFIRM')
        self.top_panel_edit_button.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: #036; }')
        self.top_panel_album_title.setText(self.album_title)
        self.top_panel_performer_name.setText(self.performer_name)
    else:
        #self.top_panel_label.setVisible(True)
        #self.top_panel_album_title_label.setVisible(False)
        #self.top_panel_performer_name_label.setVisible(False)
        self.top_panel_edit_button.setText('EDIT')
        self.top_panel_edit_button.setStyleSheet('#top_panel_edit_button { font-family: "Ubuntu"; font-size: 12pt; font-weight:bold; qproperty-alignment: "AlignCenter"; color: white; }')

        if not self.top_panel_album_title.text() == '':
            self.album_title = self.top_panel_album_title.text()
        if not self.top_panel_performer_name.text() == '':
            self.performer_name = self.top_panel_performer_name.text()
        update_info(self)

    self.top_panel_label.setVisible(not self.top_panel_label.isVisible())
    self.top_panel_album_title.setVisible(not self.top_panel_label.isVisible())
    self.top_panel_performer_name.setVisible(not self.top_panel_label.isVisible())
    self.top_panel_album_title_label.setVisible(not self.top_panel_label.isVisible())
    self.top_panel_performer_name_label.setVisible(not self.top_panel_label.isVisible())

    performer_present = False
    album_present = False
    for audio in self.list_of_audios:
        if not audio[2] == '':
            performer_present = True
        if not audio[3] == '':
            performer_present = True

    for audio in self.list_of_audios:
        if not performer_present and not self.performer_name == '':
            audio[2] = self.performer_name
        if not album_present and not self.album_title == '':
            audio[3] = self.album_title

def update_info(self):
    album_title = 'Unnamed album'
    performer_name = 'Unnamed performer'

    if not self.album_title == '':
        album_title = self.album_title
    if not self.performer_name == '':
        performer_name = self.performer_name
    self.top_panel_label.setText('<font style="font-size:8pt;">ALBUM TITLE</font><br><font style="font-size:14pt;"><b>' + album_title + '</b></font><br><font style="font-size:8pt;">PERFORMER NAME</font><br><font style="font-size:14pt;"><b>' + performer_name + '</b></font>')

    duration = 0.0
    for audio in self.list_of_audios:
        duration += audio[-1]

    if duration == 0.0:
        total_duration = 'EMPTY'
    else:
        total_duration = str(int(float(duration)/60)) + 'min ' + str(int(float(duration)%60)) + 'sec'

    self.right_panel_label.setText('<font style="font-size:8pt;">TOTAL DURATION</font><br><font style="font-size:14pt;"><b>' + total_duration + '</b></font>')

    if len(self.list_of_audios) > 0:
        self.create_ddp.setVisible(True)
        self.cdmp_list.setVisible(True)
        self.cdmp_list_alert.setVisible(False)
    else:
        self.create_ddp.setVisible(False)
        self.cdmp_list.setVisible(False)
        self.cdmp_list_alert.setVisible(True)

    if self.album_title == '':
        self.create_ddp.setVisible(False)
    else:
        self.create_ddp.setVisible(True)

def create_ddp(self):
    generate_effect(self, self.top_panel_animation, 'geometry', 500, [self.top_panel.x(),self.top_panel.y(),self.top_panel.width(),self.top_panel.height()], [self.top_panel.x(), (-1)*self.top_panel.height(),self.top_panel.width(),self.top_panel.height()])
    generate_effect(self, self.right_panel_animation, 'geometry', 500, [self.right_panel.x(),self.right_panel.y(),self.right_panel.width(),self.right_panel.height()], [-54, self.right_panel.y(), self.right_panel.width(),self.right_panel.height()])
    generate_effect(self, self.left_panel_animation, 'geometry', 500, [self.left_panel.x(),self.left_panel.y(),self.left_panel.width(),self.left_panel.height()], [(-1)*self.left_panel.width(),self.left_panel.y(),self.left_panel.width(),self.left_panel.height()])
    generate_effect(self, self.right_panel_icon_animation, 'geometry', 500, [self.right_panel_icon.x(),self.right_panel_icon.y(),self.right_panel_icon.width(),self.right_panel_icon.height()], [self.main_panel.width()*.6,(-1)*(self.main_panel.height()*.25),self.main_panel.height()*1.5,self.main_panel.height()*1.5])

    self.right_panel_label.setVisible(False)
    self.create_ddp.setVisible(False)
    self.right_panel_wait_label.setVisible(True)

    self.exec_generateddp_thread.folder = self.list_of_audios[0][0].rsplit('/',1)[0]
    self.exec_generateddp_thread.performer_name = self.performer_name
    self.exec_generateddp_thread.album_title = self.album_title
    self.exec_generateddp_thread.list_of_audios = self.list_of_audios
    self.exec_generateddp_thread.start()

def cdmp_list_populate(self):
    self.cdmp_list.clear()
    for audio in self.list_of_audios:
        icon = QtGui.QIcon(os.path.join(path_graphics, "audio_icon.png"))
        item = QtGui.QListWidgetItem(icon, '')
        self.cdmp_list.addItem(item)
        label = QtGui.QLabel('<font style="font-size:14pt; color:black;">' + audio[0].split('/')[-1] + '</font><br><font style="font-size:10pt; color:silver;">' + audio[1] + ' - ' + str("%02d" % int(int(float(audio[-1])/60))) + ':' + str("%02d" % int(int(float(audio[-1])%60))) + ':' + str("%02d" % int(int(float(str("%.2f" % float(audio[-1])).split('.')[-1])*.75))) + '</font>')
        self.cdmp_list.setItemWidget(item, label)
    update_info(self)

#def hide_panels(self):


#def show_panels(self):


def generate_effect(self, widget, effect_type, duration, startValue, endValue):
    widget.setDuration(duration)
    if effect_type == 'geometry':
        widget.setStartValue(QtCore.QRect(startValue[0],startValue[1],startValue[2],startValue[3]))
        widget.setEndValue(QtCore.QRect(endValue[0],endValue[1],endValue[2],endValue[3]))
    elif effect_type == 'opacity':
        widget.setStartValue(startValue)
        widget.setEndValue(endValue)
    widget.start()

app = QtGui.QApplication(sys.argv)
app.setStyle("plastique")
#app.setStyle('cleanlooks')
app.setApplicationName('CD Master Producer')
#app.addLibraryPath(path_cdmp)
font_database = QtGui.QFontDatabase()
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-RI.ttf'))
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-R.ttf'))
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-B.ttf'))
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-BI.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-B.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-BI.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-C.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-L.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-LI.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-M.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-MI.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-R.ttf'))
font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu-RI.ttf'))
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu)Mono-RI.ttf')
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu)Mono-R.ttf')
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu)Mono-B.ttf')
#font_database.addApplicationFont(os.path.join(path_cdmp, 'resources', 'Ubuntu)Mono-BI.ttf')
app.setFont(QtGui.QFont('Ubuntu'))
app.setDesktopSettingsAware(False)
app.main = main_window()

app.main.show()

sys.exit(app.exec_())
