from setuptools import setup
import sys, os, subprocess

APP = ['cd_master_producer.py']
NAME = str(open('debian/changelog').read()).split(' (')[0]
VERSION = str(open('debian/changelog').read()).split('(')[1].split(')')[0]
if sys.platform == 'darwin':
    OPTIONS = {'py2app': {'argv_emulation':True, 'iconfile':'pixmaps/cd_master_producer.icns',  'emulate_shell_environment':True, 'qt_plugins': 'imageformats'}}
    SETUP_REQUIRES = ['py2app']
    share_or_resources = ''
else:
    OPTIONS = {}
    SETUP_REQUIRES = []
    share_or_resources = 'share/' + NAME.lower() + '/'

DATA_FILES = []
for dirname in ['graphics', 'resources']:
    for filename in os.listdir(dirname):
        filepath = dirname + '/' + filename
        if os.path.isfile(filepath) and not filepath[-4:] == '.pyc' and not filepath.split('/')[-1] == '.DS_Store':
            DATA_FILES.append((share_or_resources + dirname, [filepath]))
        
setup(
    name=NAME,
    version=VERSION,
    app=APP,
    data_files=DATA_FILES,
    options=OPTIONS,
    setup_requires=SETUP_REQUIRES,
)

subprocess.call(['chmod', '-R', '777', 'dist/' + NAME + '.app'])
#subprocess.call(['chmod', '-R', '777', 'dist/' + NAME + '.app'])
